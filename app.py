#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, flash, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.sql import text
from datetime import datetime, timedelta

app = Flask(__name__)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/database.db' + '?check_same_thread=False'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)


###################
# Database models #
###################
class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    entry = db.Column(db.DateTime)
    leaving = db.Column(db.DateTime, default='NULL')

    def __init__(self):
        self.entry = datetime.now()
        self.leaving = datetime.fromtimestamp(0)

    def __repr__(self):
        return '<Record %r from %r to %r>' % (self.id, self.entry, self.leaving)

##################
# Admin inerface #
##################
admin = Admin(app)
admin.add_view(ModelView(Record, db.session()))

###############
# Application #
###############

# Index page with history values
@app.route('/', methods=['GET', 'POST'])
def index():
    # If sended history form, get year and month from form
    if 'show_history' in request.form:
        year = request.form['year']
        month = request.form['month']
    # Else use actual year and month
    else:
        year = str(datetime.now().year)
        month = str(datetime.now().month).zfill(2)

    # Find records related to month and year
    records = Record.query.filter(text("strftime('%m', entry)=:month AND strftime('%Y', entry)=:year")).params(month=month, year=year).order_by(Record.entry.desc())

    # Get data and other values from every record in database
    result = []
    for record in records:
        item = {}
        item['id'] = record.id
        item['date'] = record.entry.date()
        item['day'] = record.entry.strftime('%A')[:3]
        item['entry'] = record.entry.strftime('%H:%M:%S')

        # Computing spent time for non-closed entry
        if record.leaving == datetime.fromtimestamp(0):
            item['leaving'] = '-'
            item['spent_time'] = datetime.now() - record.entry
            item['today_leaving'] = (record.entry + timedelta(hours=8)).strftime('%H:%M:%S')
        else:
            item['leaving'] = record.leaving.strftime('%H:%M:%S')
            item['spent_time'] = record.leaving - record.entry

        if item['spent_time'] < timedelta(hours=7):
            item['spent_color'] = 'red'
        elif item['spent_time'] > timedelta(hours=7) and item['spent_time'] < timedelta(hours=8):
            item['spent_color'] = 'orange'
        elif item['spent_time'] > timedelta(hours=8):
            item['spent_color'] = 'green'

        result.append(item)

    # If we have no results from database, just render empty template
    if len(result) == 0:
        return render_template('index.html', items=[], year=year, month=month, computed={})
    # For one result in db render only items without computed values
    elif len(result) == 1:
        return render_template('index.html', items=result, year=year, month=month, computed={})

    # Computing max/min/average values
    computed = {}
    items_count = len([item for item in result if item['leaving'] != '-'])
    computed['month_sum'] = str(timedelta(seconds=sum([item['spent_time'].total_seconds() for item in result if item['leaving'] != '-']))).split('.')[0]
    computed['day_avg'] = str(timedelta(seconds=sum([item['spent_time'].total_seconds() for item in result if item['leaving'] != '-']) / items_count)).split('.')[0]
    computed['day_max'] = str(max([item['spent_time'] for item in result if item['leaving'] != '-'])).split('.')[0]
    computed['day_min'] = str(min([item['spent_time'] for item in result if item['leaving'] != '-'])).split('.')[0]
    computed['overtime'] = str(timedelta(seconds=sum([item['spent_time'].total_seconds() for item in result if item['leaving'] != '-'])) - timedelta(hours=8)*items_count).split('.')[0]

    # Renegerate spent time to better format
    for item in result:
        item['spent_time'] = str(item['spent_time']).split('.')[0]

    # Render template
    return render_template('index.html', items=result, year=year, month=month, computed=computed)


# Function for writing entry or leaving record to database
@app.route('/record', methods=['GET', 'POST'])
def record():
    # Find last record in db
    last_record = Record.query.order_by(Record.entry.desc()).first()

    # For entry record
    if 'entry' in request.form or 'entry' in request.args:
        # If last record hasn't leaving, error and close it first
        if last_record.leaving == datetime.fromtimestamp(0):
            flash(u"Error! Poslední záznam nebyl uzavřen!", "danger")
            last_record.leaving = datetime.now()
            db.session.commit()
        # Else save new record to database
        new = Record()
        db.session.add(new)
        db.session.commit()
    # For leaving record
    elif 'leaving' in request.form or 'leaving' in request.args:
        # If we haven't record waiting for close, error
        if last_record.leaving != datetime.fromtimestamp(0):
            flash(u"Error! Neexistuje příchod!", "danger")
            return redirect(url_for('index'))
        # Else update existing record (add leaving) and save
        else:
            last_record.leaving = datetime.now()
            db.session.commit()

    # Render OK
    flash("OK!", "success")
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
